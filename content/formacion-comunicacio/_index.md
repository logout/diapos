+++
outputs = ["Reveal"]
+++

{{% section %}}

## Taller de comunicación segura


<img src="../img/logo-gris.svg" alt="Hacklab Logout" style="max-width:40vw;border:none;background:transparent;">

---

## Quienes son los malos de la película

- La poli (físico o remoto)
- ISPs
- GAFAM / BATX
- Malhechores informáticos
- Acosadores

---

## Qué es el software libre?

Libertad para:
1. Ejecutar
2. Estudiar el código
3. Distribuir copias
4. Modificar el código

---

## Qué es la encriptación?

![Encriptación](../img/encriptacio.png)

---

![Comparativa apps mensajería](../img/apps_mensajeria.png)

---

## Servidores de correo

- sindominio.net
- riseup.net
- posteo.de

---

### Documentos compartidos

- notas.sindominio.net
- pad.riseup.net
- framapad.org
- cryptpad.fr

---

## Contacte

* **Correu** logout(arroba)sindominio.net
* **Web** [sindominio.net/logout](https://sindominio.net/logout)
* **Presentació** [sindominio.net/logout/diapos](https://sindominio.net/logout/diapos)

Salut!

---

![Comparativa apps videoconferencias](../img/apps_videoconferencias.png)

{{% /section %}}

