+++
outputs = ["Reveal"]
+++

{{% section %}}

## Taller de autodefensa digital para activistas

Descripción.

---

* Autoria
  * 2021 Hacklab Logout
* Llicència: [CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)


{{% /section %}}



{{% section %}}

## Expectativas

{{% /section %}}


{{% section %}}

### ¿Cómo funciona internet?
![NyanCat](../com-funciona-internet/img/0-portada/pirate-nyan.gif)

{{% /section %}}

{{% section %}}
### Partes de la comunicación

![Diagrama comunicación](comunicacion/comunicacion.png)

{{% /section %}}

{{% section %}}

### Tipos de vulnerabilidades

* Ingeniería social

* Vulnerabilidades de hardware

* Vulnerabilidades de software

* Vulnerabilidades de red

{{% /section %}}

{{% section %}}

### Evaluación de riesgos

{{% /section %}}


{{% section %}}

## ¿Cómo nos protegemos de...?
![Cowboy Bebop](protegernos/img/cowboy-bebop.gif)

{{% /section %}}

