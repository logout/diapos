+++
outputs = ["Reveal"]
+++

{{% section %}}

## Taller de cuidado 

Protegernos: cómo, qué y por qué


<img src="../img/logo-gris.svg" alt="Hacklab Logout" style="max-width:40vw;border:none;background:transparent;">

---

## Quienes son los malos de la película

- La poli (físico o remoto)
- ISPs
- GAFAM / BATX
- Malhechores informáticos
- Acosadores

---

## Cosas chungas

- Robo de datos: información personal identificable / sensible
- Malware: botnets, miners, ransomwares
- Ley de Murphy: cerveza encima del teclado, el perro se come el disco duro, meteoritos...

---

### Seguretat Holística

![somos](../img/somos.jpg)

---

### Glossari

* Mensaje **texto plano**: el missatge viatja sense sobre, qualsevol el pot llegir

* **Encriptación**: fer il·legible un missatge si no tens la clau
* **ISP**: proveïdor d'internet

* **HTTP(S)**: és un protocol d'internet. Unes regles que han de seguir dues parts per a poder-se comunicar.

----

* **VPN**: és un túnel para ocultar el teu origen

* **TOR**: és un navegador que dificulta conèixer l''origen del missatge

* **Router**: és la cosa que decideix cap a on van els missatges

* **Servidor**: és un ordinador que et dóna informació o ofereix un servei

---

* **Soberania tecnlogica**: Fer servir eines que ens donin el control de les nostres dades: crear-les, modificar-les i eliminar-les. 
* **Seguretat holistica**: Ens protegim entre totis i prenem consciència de la seguretat com a part de la vida.
* **FLOSS**: Software obert i lliure. Es pot inspeccionar, modificar i distribuir lliurement i el codi font es públic.
* **GAFAM**:  Google, Apple, Meta, Amazon y Microsoft. Empreses a evitar, les nostres dades no són nostres
* **0 day**: Vulnerabilitat d'un programa o dispositiu per al qual encara no hi ha solució. Es pot utilitzar per comprometre un dispositiu.


---

* **Prism-break**: Alternatives de programari lliure
* **Alternativeto**: Alternatives de programari lliure
* **F-droid**: Repositori de aplicacions lliures per a Android. Com activar f-droid
* **Aurora Store**: Baixar apps de Google Play sense registrar-se

{{% /section %}}

---

{{% section %}}

# Auto-cuidados 

### Cómo protegernos a nosotres mismes

---

### Contraseñas 
* Gestor contraseñas: keepass 

* [haveibeenpwned](https://haveibeenpwned.com/)

---

### Imagenes rrss 

#### Las imágenes subidas no son tuyas

* Reconocimiento facial

* Estudios de mercado

* Metadatos: exif

* Opciones: obscurecam

---

### Historial de ubicaciones 

#### Busca tu historial de google. ¿Dónde has estado?  

* Móviles: GPS, señal, IPs, wifi

* Actividad de Google (ubicación, búsquedas...):
	* [myactivity.google.com](https://myactivity.google.com)

---

### Navegación segura 

* HTTP vs HTTPS (S = seguro)

* TOR: navegación anónima

* VPN: túnel, no se pueden relacionar las personas con las páginas visitadas 

{{% /section %}}

---

{{% section %}}

# Cura de grup

### Com protegir-nos entre nosaltres

---

![Comparativa apps mensajería](../img/apps_mensajeria.png)

---

![Comparativa apps videoconferencias](../img/apps_videoconferencias.png)

---

### Documentos compartidos

- Pads
	- [Riseup](https://pad.riseup.net)
	- [Framapad](https://framapad.org)
	- [Cryptpad](https://cryptpad.fr)
- Fitxers: Nexcloud
	- [sindominio.net](https://sindominio.net)
- Formularios
	- [LiberaForms.org](https://liberaforms.org)

{{% /section %}}


---

{{% section %}}

# Cures de dispositius
_Cojes el moviris y lo tiras_

![cojelmovris](../img/cojelmovris.png)

---

### Vida de un activista del siglo XXI

* Smartphones
* Ordenadores
* Objetos comprometidos

![Pata de cabra](../img/pata_de_cabra.jpg)

Anem a una assamblea? A una mani?

---

## Smartphones

Insegurs per tres bandes

1. Telèfon simple
2. Sensors (smart)
3. Ordinador

---

## 1. Telefon

- GSM i geolocalització
- Escoltes
- Metadades

---

### Estem localitzades

![retencio-dades-spitz](../img/retencio-dades-spitz.png)

2009: [Malte Spitz, eurodiputat verd](http://www.zeit.de/datenschutz/malte-spitz-data-retention)


---

## 2. Smart (sensors)

- Micròfon
- GPS
- Llarg etc (llum, orientació, giroscopi, metalls...)

---

## 3. Ordinador

- Atacs a nivell de software (pegasus)
- Portes obertes a tot tipus d'alimanyes

---

### Ens fan una intervenció

- Fisica: clonar una sim, patró de desbloqueig simple, infecció sense desbloqueig, robatori.
- Remota: Pegasus, spyware, a través de la propia xarxa (cas USA, UK etc..., q fan servir la infrastructura ja que la posseeixen per espiar)

---

### Prevenció

- Fisica: está encriptat? Te una bona protecció pel desbloqueig? Tinc backup de les dades? A on?
- Remota: faig servir FLOSS? el sistema operatiu está actualitzat i es FLOSS? Quines infrastructures faig servir?

---

## Alguns consells

- Bloquejar sempre el dispositiu amb una bona contrasenya (no deixar-lo desbloquejat)
- No connectar-nos a xarxes de no confiança
- Ni a usb no confiables
- Encriptació del disc per si ens el roben
- No descarregar instaŀlables ni copiar coses amb `sudo` a la consola
- Pujar arxius a https://virustotal.com en cas de dubte
- Eines FLOSS. Descarregar des repositoris: `apt` / `pacman` o `flatpak`

---

### No em puc treure el mobil de sobre!

- Bola de jerseis insonoritzants
- Bossa de faraday
- Microones
- Dale el telefono a un gatito
- Absència d'activitat
- Mode avió

---

### Sistemes operatius lliures

- Portàtils: GNU-Linux
  - Debian
  - Linux Mint
  - Manjaro
  - Ubuntu
- Mòbils: Derivats d'Android Open Source
  - LineageOS, iode, eOS (+ mòbils compatibles)
  - Calyx, Graphene (+ seguretat hardware)

{{% /section %}}

---

{{% section %}}

## Agraïments

Moltes gràcies a DonesTech per tot el material!!
[donestech.net](https://donestech.net)

## Contacte

* **Mail** logout(arroba)sindominio.net
* **Web** [sindominio.net/logout](https://sindominio.net/logout/)

Salut!

{{% /section %}}

