+++
outputs = ["Reveal"]
+++

{{% section %}}

# Autogestion digital
##  básica

---

#### Temas que trataremos

###  Comunicación textual (mail, mensajeria, etc..)
###  Compartir archivos
###  Trabajo en común
###  Encuestas y formularios

---

## Hola mundo!
* Autoria: 2020 [Hacklab Logout] https://sindominio.net/logout/
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

---

## Roda de motivacions

* Què esperes d'aquesta sessió?
* Per què estàs aquí?

---

##### Sondeig tècnic
#### aparells electrónics
#### quins utilitzes?

* Ordenador: Windows - MacOS - GNU - Ubuntu
* Teléfon: iOS - Android

---

##### Sondeig tècnic
#### eines communicacio
#### quins utilizes?

* Mail: gmail/hotmail/yahoo/outlook - rosadefoc.info/sindominio.net/riseup
* Mensajería: Watsapp - Telegram - Signal - Jabber - Matrix
* Compartir documents: Google Drive, Dropbox, NextCloud, share.riseup
* Gestion de datos: Doodle - google Forms - croodle - framaforms - liberaforms


{{% /section %}}

---

{{% section %}}

## enviar texto
### com es pot fer?

* en una botella
* con una postal
* por carta
* sms
* mail
* mensajeria instantánea

---

## correo electrónico  

* lo público vs lo privado
* autogestion vs GAFAM
* escribir texto plano o formateado
* adjuntar archivos: si o no

---

## lo público vs lo privado
* el correo postal es inviolable
* el correo electrónico no lo es

---

## autogestion vs GAFAM
* mi vida es... mia o suya
* es gratis: tu eres el producto
* libertad de expresión: me borraron la cuenta
* control de lo que digo: censura

---

## el mail es estándar
* puedo hablar con quien quiera
* no obligo a usar determinada empresa
* puedo autogestionarlo / delegar

---

## escribir texto plano
## o formateado
* facilitar la lectura
* evitar distracciones

---

## adjuntar archivos
## vs escribir texto
* más atractivo / leo más rápido
* necesito un aplicacion / lo leo en pantalla
* ocupa más / más ligero

---

### comparación tamaño ficheros
![comparacion tamaños textos](img/tamano-ficheros-txt-rtf-pdf.png)

---

### pero no olvidemos

#### lo importante

#### es comunicar

## mandar un beso

## mandar un abrazo

{{% /section %}}

---

{{% section %}}

# compartir archivos

---

## como puedo
* Carpetas compartidas
* subir ficheros temporalmente

---

## Carpetas compartidas

NextCloud
https://probeta.net/nextcloud

---

### nextcloud probeta
![compartir ficheros](img/ajut-fitxers/probeta-nextcloud-fitxers.png)

---

#### affegeix carpeta
![compartir ficheros - affegeix-carpeta](img/ajut-fitxers/probeta-nextcloud-fitxers-affegeix-fixer-carpeta.png)

---

#### comparteix fitxers
![compartir ficheros](img/ajut-fitxers/probeta-nextcloud-fitxers-comparteix-01.png)

---

#### comparteix fitxers
![compartir ficheros](img/ajut-fitxers/probeta-nextcloud-fitxers-comparteix-02.png)

---

#### comparteix fitxers
![compartir ficheros](img/ajut-fitxers/probeta-nextcloud-fitxers-comparteix-03.png)

---

#### comparteix fitxers
![compartir ficheros](img/ajut-fitxers/probeta-nextcloud-fitxers-comparteix-04.png)

---

### ficheros temporales

* **share.riseup.net**<br/>
    max  12 horas -  50Mb
* **onionshare.org**<br/>
    comparte ficheros de manera segura y anonima

---

### https://share.riseup.net
max  12 horas - 50Gb

![fichero temporal en tmp share riseup](img/shareriseup.png)

---

{{% /section %}}

{{% section %}}

# trabajo en común
* nextcloud de [Probeta](https://probeta.net/nextcloud)
* pad: [riseup](https://pad.riseup.net)  //  [systemli](https://pad.systemli.org)
* onlyoffice/cryptpad vs gdrive/office 360

---

## probeta.net/nextcloud

* permite controlar quien y como
* tiene documentos solo texto
* mantiene las cosas del Ateneu ordenadas

---

## pad
* permite editar texto entre varias personas a la vez
* colores para distinguir personas, nombres y chat
* historial de cambios
* desaparece pasado un tiempo "automágicamente"

---

### pads en servidores libres autogestionados

* https://pad.riseup.net
* https://pad.systemli.org

---
### crear un pad de [riseup](https://riseup.net)
![pad.riseup.net](img/pad.riseup.net-01.png)

---

### pad página de inicio
![pad.riseup.net](img/pad.riseup.net-02.png)

---

### modificar tu nombre y color
![pad.riseup.net](img/pad.riseup.net-03.png)

---

### pad: enlace URL, <br/> compartir e historial
![pad.riseup.net](img/pad.riseup.net-04.png)

---

### pad: opciones e idioma
![pad.riseup.net](img/pad.riseup.net-05.png)

---

## onlyoffice/cryptpad

* texto formateado / hojas de cálculo / presentaciones
* no vende tus datos
* sin crear cuenta: documentos puntuales
* creando cuenta: varios docs ordenados en carpetas

---

### servicios de onlyoffice/criptpad

* https://cryptpad.fr

---

### cryptpad pagina inicio
#### permite elegir el tipo de documento

![cryptpad](img/cryptpad-01.png)

---

### cryptpad: documento texto

![cryptpad](img/cryptpad-02.png)

---

### cryptpad: hoja de cálculo

![cryptpad](img/cryptpad-03.png)


{{% /section %}}

---

{{% section %}}

# pidiendo datos
* mis/tus datos son mercancia
* lo gratis no es gratis, es el producto
* la seguridad importa

---

## encuestas y formularios
* encuestas autogestionadas
* formularios autogestionados

---

## encuestas autogestionadas
* https://www.systemli.org/poll/
* https://poll.disroot.org/
* https://probeta.net/nextcloud/
* http://dudle.inf.tu-dresden.de/
---
### accedir al nextcloud de probeta
#### https://probeta.net/nextcloud/
![escriptory nextcloud probeta](img/ajut-enquestes/probeta-nextcloud-01.png)

---

#### enquesta: affegeix una nova
![probeta enquestes 01](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-01.png)

---

#### enquesta: tipus
![probeta enquestes 02](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-02.png)

---

#### enquesta: configuracio
![probeta enquestes 03](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-03.png)

---

#### enquesta: affegeix opcions
![probeta enquestes 04](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-04.png)

---

#### enquesta: comparteix
![probeta enquestes 05](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-05.png)

---

#### enquesta: copia enllaç
![probeta enquestes 06](img/ajut-enquestes/probeta-nextcloud-enquestes-affegeix-06.png)

---

## formularios autogestionados
* https://liberaforms.org
* https://probeta.net/nextcloud/


---

## liberaforms.org
![liberaforms web](img/ajut-formularis/liberaforms-01-web.png)

---

#### formularis al nextcloud
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-01.png)

---

#### formulari: crea un nou
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-02.png)

---

#### formulari: affegeix pregunta
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-03.png)

---

#### formulari: edita pregunta
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-04.png)

---

#### formulari, pretunta: fes la obligatoria / suprimeix la
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-05.png)

---

#### formulari: comparteix / mira els resultats
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-06.png)

---

#### formulari: copia el enllaç per compartir
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-07.png)

---

#### formulari: configuracio
![probeta formularis 01](img/ajut-formularis/probeta-nextcloud-formulari-crea-08.png)

{{% /section %}}
