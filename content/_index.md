+++
title = "Las diapos de logout"
outputs = [ "Reveal" ]
+++

{{% section %}}

# Las diapos de Logout

Estos son nuestros papiros

---

## Formacions de seguretat digital


* <a href="https://humo.sindominio.net/s/JBpiPALQPRbbApp" target="_blank">'24 - Seguridad para smartphones [PDF]</a>
* <a href="formacion-autodefensa" target="_blank">'23-'24 - Seguridad digital para activistas</a>
* <a href="inseguretat-smartphone" target="_blank">'16-'21 - Inseguretat smartphone</a>
* <a href="seguretat-privacitat" target="_blank">'17-'21 - Eines de seguretat i privacitat</a>
* <a href="seguretat-digital-per-a-sindicats-combatius" target="_blank">'20 - Seguretat digital per a sindicats combatius</a>
* <a href="encriptacio-de-dispositius" target="_blank">'20 - Encriptació de dispositius</a>

---

## Formacions de sobirania tecnològica

* <a href="autogestio-digital" target="_blank">'21-'23 - Autogestió digital bàsica</a>
* <a href="com-funciona-internet" target="_blank">'21 - Infraestructura d'internet</a>

---

## Xerrades

* <a href="superordinadors-fuste" target="_blank">'24 - Fusteria - Superordinadors</a>
* <a href="formacion-comunicacio" target="_blank">'23 - Taller de comunicación segura</a>
* <a href="taula-collapse" target="_blank">'22 - Taula rodona sobre el coŀlapse</a>
* <a href="autodefensa-activistas" target="_blank">'21 - Autodefensa de activistas</a>

{{% /section %}}
