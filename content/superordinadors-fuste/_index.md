+++
outputs = ["Reveal"]
+++

{{% section %}}

## Superordenadores

La Fustería 15 de noviembre de 2024

{{% /section %}}

---

{{% section %}}

## Servidor

![servidor](img/servidor.png)

---

## Centro de datos

![centro-datos](img/centro-datos.jpg)

---

## Superordenador

![frontier](img/frontier.jpg)

---

## ¿Comó medimos la potencia de un ordenador?

FLOPS

**FL**oating point **O**perations per **S**econd

(Operaciones de punto flotante por segundo)

---

## Móvil vs Superordenador

| | Móvil | Superordenador |
|-|-------|------------|----------------|
| Procesadores | 4/8 | 64/128 |
| Nodos | 1 | +10.000 |
| FLOPS | 500 **G**FLOPS | 1200 **P**FLOPS |

---

## Top500

<img src="img/top500.png" alt="top500" style="height:60vh;"/>

---

## Marenostrum 5

![mn5](img/mn5.jpg)

---

## Marenostrum 5

- 8.000 nodos, cada nodo:
    - CPU: Intel Sapphire Rapids X 2
    - GPU: Nvidia Hopper X 4
    - 512 GB de memoria
- 20.400 discos duros de 18 TB = 248.000 TB
- 600 routers aprox.
- Refrigeración por agua

{{% /section %}}

---

{{% section %}}

# Aplicaciones

---

## Predicción meteorológica

![clima](img/clima.jpeg)

---

## Simuladores de corazón

<img src="img/corazon.jpg" alt="corazon" style="height:60vh;"/>

---

## Alineadores de secuencias

- Genómica:
    - Encontrar mutaciones
    - Células cancerígenas
    - Diferencias entre especies
- Shazam!!

<img src="img/adn.png" alt="adn" style="height:28vh;"/>

---

## Pero también...

- Programas para encontrar petroleo (Repsol)
- Simuladores de aviones y cohetes (UE, INDRA)
- Reconocimiento facial (ACAB)

{{% /section %}}

---

{{% section %}}

# Consecuencias políticas

---

## Consumo de agua

Tu Nube Seca mi Río (tunubesecamirio.com)

![agua](img/agua.png)

---

## Inversión de Backstone

![blackstone](img/blackstone.png)

---

## Algoritmos sobredimensionados

- Algoritmos de recomendación
- ChatGPT
- Retransmisiones en 4K

¿Tienen sentido?

---

## Soberanía tecnológica

- Propiedad de los centros de datos
- Hardware cerrado
- Aplicaciones privativas

{{% /section %}}

---

{{% section %}}

# Alternativas

- Lowtech: Instalar un pequeño servidor en casa
- Servidores autogestionados: sindominio.net, riseup.net, framasoft.org
- Hardware libre: RISC-V

{{% /section %}}

