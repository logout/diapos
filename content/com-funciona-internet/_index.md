+++
outputs = ["Reveal"]
+++

{{% section %}}

# Com funciona internet?
![nyan cat disfressat de pirata](img/0-portada/pirate-nyan.gif)

---

## Ei hola!
* Autoria: 2021 [Hacklab Logout](https://sindominio.net/logout/)
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

{{% /section %}}

---

{{% section %}}

## Què forma internet...?

### Coses que hem sentit

---

### Satèŀlits!

![un satèŀlit sobre la Terra](img/1-topics/satellit.jpg)

---
### Antenes!

![matriu d'antenes en una sola estructura](img/1-topics/antena-array.webp)

---
### "Energia"

![bosc amb colors irisats i feixos d'energia](img/1-topics/bosc-magic.jpg)

---

_Hi ha una mica de veritat en tots, però sobretot tenim_

* fibra òptica terrestre
* fibra òpitca marítima
* centres d'interconnexió d'operadores
* centres de dades / granges de servidors
* mòdems òptics
* routers, switchs, brigdes

{{% /section %}}

---

{{% section %}}

## Fibra òptica terrestre

---

### Fibra òpitca per vies de tren I

![foto de paret amb cables i tren de rodalies](img/2-fibra-terra/fibra-adif.jpg)

---

### Fibra òpitca per vies de tren II

![foto de via d'ave amb opearis amb rulo de fibra](img/2-fibra-terra/fibra-ave.jpg)

---

### Fibra òpitca per carreteres i autopistes

![foto de bobina de tubs per tirar-hi fibra (DIBA)](img/2-fibra-terra/fibra-carretera.jpg)

---

### Fibra òpitca per clavegueram

![foto de claveguera amb cables a la paret (clabsa)](img/2-fibra-terra/fibra-claveguera.jpg)

---

### Fibra aèria (amb teixit elèctric)

![operari instaŀlant cable de fibra òptica en una torre elèctrica](img/2-fibra-terra/fibra-aire.jpg)

---

### Fibra aèria d'accés final

![tres torpedos de fibra en una façana](img/2-fibra-terra/fibra-paret.jpg)

---

### Exemples de companyies de "fibra fosca"
![dibuix esquemàtic: teixit elèctric i vies de tren](img/2-fibra-terra/reintel-esquema.gif)

---

#### Reintel (red eléctrica i adif) - 50m km
[![mapa de xarxa de Reintel](img/2-fibra-terra/mapa-reintel.png)](https://www.reintel.es/es/infraestructuras/mapa-red)

---

#### Lyntia (gas natural, endesa, iberdrola) - 40m km

[![mapa de xarxa de Lyntia](img/2-fibra-terra/mapa-lyntia.png)](https://www.lyntia.com/red-fibra-optica/)


{{% /section %}}

---

{{% section %}}

## Fibra òptica marítima

---

### Esquemaa d'instaŀlació
(cable submarí elèctric, no de fibra)

![esquema d'instaŀlació d'un cable submarí elèctric valència-mallorca](img/3-fibra-mar/esquema-installacio.jpg)


---

### Cable en el terra marí

![robot ha trobat la secció trencada de cable submarí](img/3-fibra-mar/sudafrica-cable-trencat.jpg)

---

### Cable reparat en el vaixell

![operaris carreguen el cable reparat en un vaixell](img/3-fibra-mar/sudafrica-cable-reparat.jpg)

---
### Bobina de cable en el vaixell instaŀlador

![bobina de cable de vaixell instaŀlador](img/3-fibra-mar/marea-bobina-vaixell.jpg)

---

### Cable arriba a la platja

![foto d'operaris a la platja, cable que ve del mar, boies i vaixell al fons](img/3-fibra-mar/cable-platja1.jpg)

---

![foto d'operari amb cable protegit a la plajta](img/3-fibra-mar/cable-platja-bilbo.jpg)


---

### Mapes de cables submarins

---

#### Telefónica

[![mapa de xarxa telefónica interacional](img/3-fibra-mar/mapa-telefonica.jpg)](https://www.wholesale.telefonica.com/en/about/international-network/)

---

#### Telxius

![mapa de la xarxa de telxius](img/3-fibra-mar/mapa-telxius.png)

---

#### Mapa Atlàntic Europeu

[![mapa de cables submarins europeus mirant cap a amèrica](img/3-fibra-mar/cablemap-america.png)](https://www.submarinecablemap.com/)

---

#### Mapa Mediterrani

[![mapa de cables mediterranis mirant cap a àsia](img/3-fibra-mar/cablemap-asia.png)](https://www.submarinecablemap.com/)

{{% /section %}}

---

{{% section %}}

## Equipaments

---

### Centraletes

---

### Punts d'interconnexió

---

### Centres de dades

{{% /section %}}

---

{{% section %}}

## Dispositius

---

### Routers i switchs

---

### Servidors

---

### Mòdems


{{% /section %}}

---

{{% section %}}

## Internet per mòbil

---

### Xarxa ceŀlular

---

### Estacions base

- rural
- urbana
- túnel/tren/autopista

{{% /section %}}

---

{{% section %}}

## Altres tecnologies d'accés

---

### Coure (X/A/VDSL)

---

### Satèŀlit

{{% /section %}}

