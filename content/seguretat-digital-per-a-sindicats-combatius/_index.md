+++
outputs = ["Reveal"]
+++

{{% section %}}

# Seguretat digital
## per a Sindicats combatius

---

## Índex

* **Bloc 1**: _Necessitat de polítiques de seguretat i autonomia_
  1. Antirepressió digital
  2. Vigilància en el lloc de treball
  3. Anticapitalisme digital
* **Bloc 2**: _Eines i pràctiques d'informació i comunicació_

---

## Hola!
* Autoria: 2020 Hacklab Logout
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

---

## Roda de motivacions

«Què esperes d'aquesta sessió?»

«Per què estàs aquí?»

---

## Sondeig tècnic

* Mail: gmail - riseup
* OS: windows - ubuntu
* Anon: Tor, metadades, cares
* Mòbil: precaucions?

---

## Seguretat
* Relativa al context
* De grup
* Psico-social + física + info
* Progressiva
* Objectiu: activisme sostenible

{{% /section %}}

---

{{% section %}}

## Bloc 1
Necessitat de polítiques de seguretat i sobirania digital

{{% /section %}}

---

{{% section %}}

## 1. Autodefensa digital antirepressiva

Idea central:  
**sobirania-control de la informació**

---

## Persecució i inflitracions policials
- S'han donat casos recents d'intents d'espionatge per part dels mosos [1](https://directa.cat/comunicacions-suplantades/) [2](https://directa.cat/interior-obre-una-investigacio-interna-a-mossos-arran-de-les-informacions-sobre-la-trama-dinfiltracio-digital/)
- Les millor solucions es tenir control de les nostres infrasetructures de comunicació
- Tenir una bona cultura de la seguretat dins el sindicat


---

## Accions al carrer
![mosso d'esquadra amb càmera de pit](img/camara_mossos.png)

---

### Perills en accions com piquets o vagues
- Identificació mitjançant sistemes de reconeixement facial
- Identificació a través del nostre dispositiu mòbil


---

## Posibles mitigacions o solucions
- Tapar-se la cara/cap
- Maquillatges / Pentinats contra reconeixement facial [1](https://www.xataka.com/robotica-e-ia/enganar-a-sistemas-reconocimiento-facial-relativamente-facil-sabes-como)
- No portar el mòbil a sobre
- Comunicació sense aparells digitals

---

## Protecció entre companyes
- afiliació secreta
- consultes amb info sensible
  - com ens comuniquem / atenem / gestionem
  - com i on guardem les dades, o on en queda rastre

---

## Motius
- Que el sindicat sigui segur i inspiri confiança
- Donar suport a les companyes que no estan cobertes per drets sindicals.
- Protegir-nos millor avui
- Avançar-nos a la repressió de demà

---

## Com tractar dades personals
![cgtsrv](img/cgt_server.jpg)
- Guardar les dades en una infraestructura pròpia (a ser posible)
- No deixar contrasenyas en post-it, ni notes d'informació imporntat sobre la taula

---

- Evitar fer les comunicacións a traves del mail/dispositius d'empresa
- Guardar aquestes dades encriptades de la forma mes segura possible
- No facilitar informació a NINGÚ del que no estem 100% segurs de la seva identitat
- No guardar dades sensibles en fitxers al escriptori/ mis documentos


{{% /section %}}

---

{{% section %}}

## 2. Espionatge dins l'empresa
![boss spy](img/boss_spy.jpg)

---

## Ordinador d'empresa

- Hi ha moltes empreses que comercialitzen software espia
- Poden tenir control de moltes coses del ordinador, fins i tot registrar tot el que escrius.
- [Article ampliant informació](https://turtler.io/news/16-worst-and-most-extreme-ways-employers-are-spying-on-their-people)
- [Taula compartiva de software espia](https://www.pcmag.com/picks/the-best-employee-monitoring-software)
- Lo mes segur es no fer servir l'ordinador de la feina per asumptes personals/sindicals

---

## Android/Mòbil

![android logo with devil horns](img/android_evil.png)
- El mateix que al ordinador amb el risc afegit de la geolocalització
- [Article sobre les millors eines espies](https://www.suspekt.org/best-spy-apps/)
- [Qustodio](https://www.qustodio.com/en/) es un example aqui a barcelona
- Lo mes segur es no fer servir el movil de la feina per asumptes personals/sindicals

---

## Mail d'empresa

![gmail logo with warning symbol](img/gmail_warning.png)

- Si es gmail normalment poden llegirlo
- Els servidors de mails gestionats per la empresa es menys probable que els vigilin
- El mes segur com sempre es no fer servir el mail de empresa per asumptes personals/sindicals

---

### _«Com puc saber si la empresa m'està espiant?»_

---

![enterprise team with formal people and a guy with beard and black shirt: "spot the linux sysadmin"](img/sysadmin_meme.png)

{{% /section %}}

---

{{% section %}}

## 3. Anticapitalisme... digital

Idea central: **no són amics, són l'enemic de sempre**

* **Cotitzen** per sobre de 🛢 🏦 👕 💊 🛦
* Caixa d'eines de **vigilància massiva**.

---

[Empreses que més cotitzen 2020](img/largest-corps-2020.jpg)
[font: howmuch.net](https://howmuch.net/articles/biggest-brands-of-the-world-2020)

---

## Big Tech USA
![Google Apple Facebook Amazon Microsoft](img/GAFAM.png)

---

## GAFAM+N

- Google
- Amazon
- Facebook
- Apple
- Microsoft
- ... Netflix

---

## Big Tech Xina
![Baidu Alibaba Tencent Xiaomi](img/BATX.jpg)

---

## BATX+H
- Baidu
- Alibaba
- Tencent
- Xiaomi
- ... Huawei

---

## Ideologia
**Estat-ciutadà**.
* Paternalisme
* Control total
* Serveis i infraestructures "públiques"
* Vigilància i repressió
* Imperialisme

---

## Capitalisme "coŀlaboratiu"

Startups amb fons de capitals riscs
- Über / Cabify
- Glovo / Deliveroo
- AirBnB

---

## Ideologia
**Plataformes d'explotació coŀlaborativa**

* Ni produir ni vendre
* Plataforma i la base d'usuàries
* Falsos autònoms "emprenedors"
* Algorismes dèspotes (crèdit social).

---

![amazon prime esprem](img/amazon-exprime.png)

---

## Amazon
* Anti-sindical [1](https://www.lavanguardia.com/economia/20201126/49710020845/amazon-espiar-trabajadores-sindicatos-black-friday.html) [2](https://www.businessinsider.es/amazon-usando-espias-pinkerton-evitar-sindicalizacion-761549)
* Magatzems de productes de consum ([amazon.com](https://amazon.com))
* Magatzems de serveis d'internet ([aws.com](https://aws.com))
  * Lidera la centralització de servidors
  * Eines de guerra (video-control de drons)
* més info a [fuckoffamazon.info](https://wiki.fuckoffamazon.info/doku.php?id=what_s_wrong_with_amazon)

---

[google monopoly](img/google-monopoly.jpg)

---

## Google
Transhumanisme + ordenació de "tota la informació"
* la nostra **xarxa social**, de contactes
* **què pensem**, ens preocupa
* **què diem**, comprem, on viatgem
* on som, **on anem**, què fem
* les cares de **qui estimem**
* patrons de son, salut, **malalties**

---

## Fonaments

Idees de dominació ([tecnopatriarcapitalisme](https://www.teixidora.net/wiki/Tecnopatriarcapitalisme_2020/02/29))
* L'Home fora i sobre de la natura
* L'individu fora i sobre de la comunitat
* La ment fora i sobre del cos

Valors de l'home BBVAH, "bolet".

---

## Conseqüències

* Progrés-creixement
* Extractivisme
* Consumisme hedonista
* Violències discriminants
* Individualisme

---

[Artista: Miguel Brieva](https://clismon.com/)

![el mundo por miguel brieva](img/el-mundo-brieva.jpg)

---

## Interseccions
- anticolonials
- feministes
- ecologistes

{{% /section %}}

---

{{% section %}}

## Bloc 2
### Eines i pràctiques d'informació i comunicació digitals

---

[Mapa de cables de fibra submarins, terrestres, i centres de dades o punts d'interconnexió](img/infrapedia-mediterrani-oest.png)

[font: infrapedia](https://www.infrapedia.com/app)

---

## Internet

- Centres de dades
- Punts d'interconnexió
- Cables submarins, autopistes, trens

---

## Informació
- **informació** estàtica / en repòs, metadades, identificable, sensible.
- **identitats** legals / anònimes / pseudònimes / coŀlectives

---

[Quatre estratègies de protecció de les identitats](img/estrategias-identidad.png)

[font: spideralex](https://myshadow.org/materials)

---

## Estratègies de protecció
- Compartimentar
- Fortificar
- Reduir
- Confondre

---

## Eines
- criteris: qui són, seguretat, xarxa...
- algunes recomanacions:
  - mail d'empresa i/o gmail → mail propi o activista o contractat
  - whatsapp → mail segur o signal.

---

## Més en detall
Consultar les diapositives de la [Formació en seguretat i privacitat](https://gitpitch.com/fadelkon/formacio-seguretat-privacitat/master?grs=gitlab)

{{% /section %}}

---

{{% section %}}

## Contacte

* **Mail** logout(arroba)sindominio.net
* **Web** [https://sindominio.net/logout/](https://sindominio.net/logout/)

Salut!

{{% /section %}}
