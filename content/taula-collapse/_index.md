+++
outputs = ["Reveal"]
+++

{{% section %}}

# Coŀlapse en disputa
## Consciència i acció

28-30 de gener, 2022  
a l'ateneu l'harmonia

---

## Taula rodona
### Eines per una sortida
### no individualista al coŀlapse (II)

Intervenció del Hacklab Logout

{{% /section %}}

---

{{% section %}}

### Pregunta inicial

## Aspectes i reptes
## davant l'emergència

---

- Tecnologia autònoma
- Xarxes de suport mutu
- Desconstrucció d'usos

{{% /section %}}

---

{{% section %}}

# Crítica

---

## Consumisme i complexitat

- "taula periòdica" coltan, terres rares, or...
- tsmc (fàbriques xips - poques)
- export petjada ecològica
- 5G, Internet of Things, Smart cities

_Apple_

---

## Estats digitals

- identitat
- serveis públics
- normes pròpies
- expansió i control de territori
- registres enornmes (big data)

_Google_

---

## Plataformes socials

- addictives/depressives
- privacitat i mineria de da
- treball gratuït
- agora pública sí, però com?

_Facebook, Netflix_

---

## Uberització

- Comprar per amazon
- Deliveroos i etc.
- Plataformes que no fan res

_Amazon_

---

## Monopolis de mercat

- Governs que compren
- Llicències gratis (escoles)
- Tractes amb fabricants
- Codi privatiu
- Efecte xarxa i pressió social

_Microsoft, Google_

{{% /section %}}

---

{{% section %}}

# Propostes

---

## Decreixement

- No fustigar-se
- Renovar màquines
  - Alternatives + sostenibles
  - Segona mà
- Allargar vida i reparar

---

## Apps lliures (I)

- Windows → GNU-Linux<br/> (linux mint/ xubuntu / manjaro)
- Whatsapp → Signal, Element, Jabber/Conversations/Dino
- Gmail → Riseup, Sindominio, Autistici
- PlayStore → F-Droid + AuroraStore

---

## Apps lliures (II)

- Chrome → Firefox
- GMaps → OpenStreetMaps / OsmAnd / OrganicMaps
- Zoom/Skype → Jitsi / Big Blue Button
- Chess.com → lichess.org ;)

---

## Individu vs Coŀlectiu

- Tallers de costura
- Tallers de bicis
- Jornades de reparació
- Hacklabs!

---

## Projectes lliures

- Altermundi, Guifinet: _xarxes_
- Sindominio, Riseup, Framasoft, Autistici: _servidors_
- Apache, Eclipse: _fundacions_
- FSF, Tor Project, Guardian Project, Framasoft

---

## Tecs post-coŀlapse

- Autoproducció d'energia
- No internet → p2p / offline / autogestió
- Low tech (webs, aplicacions, servidors)
- Offline: kiwix, OsmAnd.
- Ferralla: Z80, Pentium IV (CollapseOS)


{{% /section %}}

---

{{% section %}}

### Pregunta de clausura

## Com anticipar-nos o 
## evitar el coŀlapse 
## des de les lluites actuals?

---

- Necessitats
- Llibertats
- Comunitat
- Seguretat

{{% /section %}}

--- 
{{% section %}}

<img src="../img/logo-gris.svg" alt="Hacklab Logout" style="max-width:40vw;border:none;background:transparent;">

- Dijous 19h - 21h
- Can Vies, Plaça de Sants, Barcelona
- logout@sindominio.net
- https://sindominio.net/logout

{{% /section %}}

