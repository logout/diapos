#!/usr/bin/octave
lon=[8:30];
char=[24:100];
[xx,yy] = meshgrid(char, lon);
meshc(xx,yy, log2(xx.^yy))
%zlabel("Complexitat en bits")
%ylabel("Longitud en caràcters")
%xlabel("Caràcters diferents possibles")
% Perquè no es mori el programa
input("")
