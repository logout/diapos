+++
outputs = ["Reveal"]
+++

{{% section %}}

## Formació en seguretat i privacitat

Valors, eines i conceptes per protegir la privacitat i
la seguretat de la informació.

---

* Autoria
  * 2020 Hacklab Logout
  * 2018 fadelkon
  * 2017 críptica (charlieMKR i fadelkon)
* Llicència: [CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

{{% /section %}}

---

{{% section %}}

## Criteris per triar eines de comunicació

---

## Topologia de xarxa
* __Peer to Peer o Friend to Friend:__ No calen servidors, els ordinadors es connecten entre ells _(Retroshare, Jami, Tox, Ricochet)_
* __Descentralitzada:__ La comunicació depèn de servidors, que es coordinen entre ells _(e-Mail, Matrix, Jabber, Mastodon)_
* __Centralitzada:__ La comunicació depèn de servidors i són gestionats per una sola entitat _(Signal, Whatsapp, Instagram)_

---

## Més tipus de (des)centralització
* __d'arquitectura__: Quantes màquines hi ha i a quants llocs són?
* __política__: Qui pren les decisions? Quants òrgans?
* __lògica__: Des de fora, funciona com un bloc uniforme o com un eixam divers?

Vitalik Buterin: [The Meaning of Decentralization](https://medium.com/@VitalikButerin/the-meaning-of-decentralization-a0c92b76a274)

---

## Informació en moviment

* __Extrem a extrem:__ Les dades es xifren a la màquina de l'emisor i es desxifren a la del receptor _(Signal)_
* __Punt a punt:__ Les dades es xifren per viatjar d'una màquina a una altra pero es desxifren en punts intermedis _(Webs HTTPS, Telegram, e-Mail)_
* __En clar:__ Les dades no es xifren en cap moment i viatgen "tal qual" _(DNS, Whatsapp pre-2012, webs HTTP)_

---

## Informació en respòs
* __Sistema operatiu__: Discos durs xifrats quan no estan en ús _(LUKS, Bitlocker, VeraCrypt, Android xifrat)_
* __Dispositiu usuari__: Xifrada en el dispositiu de la usuària amb una contrasenya mestra _(Signal mòbil, Firefox, Thunderbird)_
* __Servidors privats__: Informació xifrada en el servidor amb la contrasenya d'usuària _(Posteo, Tutanota)_
* __Servidors comunitaris__: Màquines no externalitzades. Control físic de la infraestructura _(Sindominio, Riseup)_

---

## Privacitat per disseny vs. per política
* __Per disseny:__ El propi disseny del programa fa que les dades no es puguin accedir pel proveïdor _(Tor)_
* __Per política:__ Les dades són accessibles pel proveïdor, però la seva política és no fer-ho, o esborrar-les _(Telegram)_
* __Cap:__ «Recollim algunes dades per millorar el servei i les compartim amb empreses associades» _(GMail, TikTok, Instagram)_

---

## Llicència del codi font
* __Lliure:__ Públic i revisable. Es pot modificar i se'n poden distribuir versions alternatives _(Firefox, Revolt)_
* __Obert__: Públic i revisable. Es pot combinar amb codi privatiu i fer-ne versions tancades. _(Android)_
* __Codi disponible__: Disponible amb restriccions d'ús, normalment d'explotació comercial _(Elastic Search)_
* __Privatiu:__ Inaccessible. Funcionament dissenyat i errors són opacs _(Google Chrome, Whatsapp)_

---

## Abast de la protecció
* __Dades i la majoria de metadades:__ Tant el contingut dels missatges com la info de suport estan protegides. Es pot deduir molt poc _(Signal)_
* __Dades:__ El contingut dels missatges està protegit, però certa informació de suport queda a la vista: emissor, receptor, graf social, horaris... _(Matrix, e-mail amb GPG)_
* __Cap:__ El contingut dels missatges i tota la informació de suport és accessible per entitats diferents a l'emissora i al receptora _(Telegram, e-mail sense xifrar)_

---

## Motivació i retribució
* __Social/cooperatiu__: Resol una necessitat social de forma sostenible per qui hi treballa _(Posteo, Pangea, Maadix)_
* __Voluntàries__: Resol necessitats socials com a activisme no remunerat _(Sindominio, Riseup)_
* __Ànim de lucre honest__: Resol algunes necessitats però vol créixer i augmentar beneficis de forma capitalista _(Tutanota)_
* __Ànim de lucre trampós__: La necessitat que treballa és només un esquer per fer aconseguir atenció, dades _(Gmail)_


---

## Taula resum 1/3

Concepte | Desitjat | Acceptable   | Inacceptable
---------|----------|--------------|-------------
Topologia de xarxa | P2P. F2F|Federada o centralitzada| "Gàbia d'or"
Xifrat | Extrem a extrem | Punt a punt | Cap xifrat
Informació protegida | Dades i metadades | Només dades | Cap protecció

---

## Taula resum 2/3

Concepte | Desitjat | Acceptable   | Inacceptable
---------|----------|--------------|-------------
Model de desenvolupament | Comunitari | Individual. Empresarial | -
Retribució | Cooperatiu-social | Voluntaris. Ànim de lucre honest | Ànim de lucre trampós
Llicència | Codi lliure| Codi obert / disponible| Codi privatiu

---

## Taula resum 3/3

Concepte | Desitjat | Acceptable   | Inacceptable
---------|----------|--------------|-------------
Opcions de privacitat | Per defecte | Opcionals | Inexistents
Protecció | Per disseny | Per política | Extractivisme de dades

---

## Altres aspectes a avaluar
* Impacte ecològic
* Inclusivitat, accesibilitat
* Impacte social
* Comunitat i documentació


{{% /section %}}

---

{{% section %}}

### "AppStore" : Fdroid
![Fdroidlogo](fdroid/img/logo.png)

---

![FdroidScreenshot](fdroid/img/screenshot1.png)

---

### PROS:

- Software libre
- Todo el contenido es software libre
- Gratis
- Sin publicidad ni rastreo

---

### CONTRES

- Les aplicacions s'han d'actualitzar manualment
- Algunes aplicacions no estan actualitzades

{{% /section %}}

---

{{% section %}}
### Missatgeria instantània: Element
![ElementLogo](element/img/logo.png)

---

### Element és
Un sistema de missatgeria instantània __multiplataforma__ de codi __lliure__ amb una arquitectura descentralitzada i __federable__.

Hi ha un __servidor "central"__ gestionat per una fundació però et pots fer el teu __propi servidor__.

Es pot configurar perquè els missatges tinguin xifrat **extrem a extrem**

---

### PROS:
* Xifrat extrem-a-extrem per text i multimèdia
* Codi obert, tant la aplicacio com el client
* Pots crear comptes 100% anònims, no vinculats al número de telèfon
* Arquitectura descentralitzada i federable
* Salas de discusión publicas
* Cliente multiplataforma, incluída versión web, accesible desde el navegador

---
### CONTRES:
* No protegeix les metadades
* No es tan fàcil de fer servir com altres aplicacions
* No xifra l'emmagatzematge local al mòbil
* El xifrat s'ha d'activar a cada sala/conversa manualment

---
### Servidores Alternativos
* [Sindominio](https://chat.sindominio.net/)
* [Guifinet](https://matrix.guifi.net/)
* [Llistat de servidors no oficials](https://www.hello-matrix.net/public_servers.php)


{{% /section %}}

---

{{% section %}}

### Missatgeria instantània: Signal
![Signal](signal/img/signal-logo.png)

---

#### Signal és
Un sistema de missatgeria instantània semblant a Whatsapp i a Telegram. Proporciona seguretat **extrem a extrem** i està gestionada per una associació sense ànim de lucre.

---

### PROS:

* Privacitat per disseny amb les dades.
* Xifrat extrem-a-extrem per text i multimèdia.
* Altament usable, semblant a Whatsapp.
* Codi i protocol de xifrat obert.
* Autodestrucció de missatges opcional
* Bloqueja els missatges si les claus de xifrat han canviat i les claus es poden comprobar fàcilment.
* Genera molt poques metadades. El servidor sap molt poc.

---

### CONTRES:

* Registre amb número de telèfon mòbil: no anònim, altament regulat
* Sistema 100% centralitzat.
* Només el client és de codi lliure, no et pots fer el teu propi servidor i federar

---
![Signal per Android](signal/img/signal-android.jpg)

---

![Captura de pantalla de Signal demostrant la funcionalitat d'esborrar cares amb una foto de manifestants](signal/img/signal_blur.png)

---

##### Petició de Dades per part del FBI, publicada per OWS i ACLU (Octubre 2016)
![La petició del FBI](signal/img/subpoena-fbi-signal.png)
* OWS: Open Whisper Systems. Associació que desenvolupa i manté Signal.
* ACLU: American Civil Liberties Union. Organització a favor dels drets civils dels USA.

---

#### La informació proporcionada per OWS
![La informació que tenia OWS](signal/img/signal-subpoena-reply.png)

[Més informació](http://arstechnica.com/tech-policy/2016/10/fbi-demands-signal-user-data-but-theres-not-much-to-hand-over/) (en anglès)


{{% /section %}}

---

{{% section %}}

## Metadades
MAT, ScrambledExif i Signal

![logo de scrambled-exif](metadata/img/scrambled_exif.png)


---

### Què són les metadades
- Són dades que donen informació sobre altres dades
- Exemple de metadades en un missatge:
  - Hora
  - Ubicació
  - Dispositiu

---

### Tipus de metadades

- **Explícites** (escrites)
  - fotos i vídeos: model de càmera
  - correu: remitent, hora
  - web: versió del navegador, sistema
- **Implícites** (deduïbles)
  - telefonia: durada d'una trucada
  - text: estil d'escriptura

---
### Vegem-ne un exemple

![Nens menjant gelat](metadata/img/nens_gelat.jpg)

---
## Metadades EXIF, JFIF, XMP...
... amb més informació del que sembla!
```
mat2 -s nens_gelat.jpg
[+] Metadata for nens_gelat.jpg:
    Aperture: 2.2
    ApertureValue: 2.2
    BlueMatrixColumn: 0.14307 0.06061 0.7141
    BlueTRC: (Binary data 2060 bytes, use -b option to extract)
    BrightnessValue: 4.68
    CMMFlags: Not Embedded, Independent
    CircleOfConfusion: 0.005 mm
    ColorSpace: sRGB
    ColorSpaceData: RGB
    ComponentsConfiguration: Y, Cb, Cr, -
    ConnectionSpaceIlluminant: 0.9642 1 0.82491
    CreateDate: 2016:04:17 16:25:24
    CreatorTool: Aperture 3.6
    DateCreated: 2016:04:17 16:25:24
    DateTime: 2016:04:17 16:25:24
    DateTimeDigitized: 2016:04:17 16:25:24
    DateTimeOriginal: 2016:04:17 16:25:24
    DeviceAttributes: Reflective, Glossy, Positive, Color
    DeviceManufacturer: Hewlett-Packard
    DeviceMfgDesc: IEC http://www.iec.ch
    DeviceModel: sRGB
    DeviceModelDesc: IEC 61966-2.1 Default RGB colour space - sRGB
    ExifByteOrder: Big-endian (Motorola, MM)
    ExifImageHeight: 500
    ExifImageWidth: 889
    ExifVersion: 0220
    ExposureCompensation: 0
    ExposureMode: Auto
    ExposureProgram: Program AE
    ExposureTime: 1/120
    FNumber: 2.2
    FOV: 60.3 deg
    Flash: No Flash
    FlashpixVersion: 0100
    FocalLength: 4.8 mm
    FocalLength35efl: 4.8 mm (35 mm equivalent: 31.0 mm)
    FocalLengthIn35mmFormat: 31 mm
    GPSAltitude: 18 m Below Sea Level
    GPSAltitudeRef: Below Sea Level
    GPSDateStamp: 2016:04:17
    GPSDateTime: 2016:04:17 20:25:10Z
    GPSLatitude: 40 deg 42' 56.07" N
    GPSLatitudeRef: North
    GPSLongitude: 73 deg 50' 36.35" W
    GPSLongitudeRef: West
    GPSPosition: 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
    GPSTimeStamp: 20:25:10
    GPSVersionID: 2.2.0.0
    GreenMatrixColumn: 0.38515 0.71687 0.09708
    GreenTRC: (Binary data 2060 bytes, use -b option to extract)
    HyperfocalDistance: 2.25 m
    ISO: 64
    LightSource: Unknown
    LightValue: 9.8
    Luminance: 76.03647 80 87.12462
    Make: samsung
    MaxApertureValue: 2.2
    MeasurementBacking: 0 0 0
    MeasurementFlare: 0.999%
    MeasurementGeometry: Unknown
    MeasurementIlluminant: D65
    MeasurementObserver: CIE 1931
    MediaBlackPoint: 0 0 0
    MediaWhitePoint: 0.95045 1 1.08905
    MeteringMode: Center-weighted average
    Model: SM-G900V
    ModifyDate: 2017:01:22 13:30:20
    Orientation: Horizontal (normal)
    PrimaryPlatform: Microsoft Corporation
    ProfileCMMType: Linotronic
    ProfileClass: Display Device Profile
    ProfileConnectionSpace: XYZ
    ProfileCopyright: Copyright (c) 1998 Hewlett-Packard Company
    ProfileCreator: Hewlett-Packard
    ProfileDateTime: 1998:02:09 06:49:00
    ProfileDescription: sRGB IEC61966-2.1
    ProfileFileSignature: acsp
    ProfileID: 0
    ProfileVersion: 2.1.0
    Rating: 1
    RedMatrixColumn: 0.43607 0.22249 0.01392
    RedTRC: (Binary data 2060 bytes, use -b option to extract)
    RenderingIntent: Perceptual
    ScaleFactor35efl: 6.5
    SceneCaptureType: Standard
    SceneType: Directly photographed
    SensingMethod: One-chip color area
    ShutterSpeed: 1/120
    ShutterSpeedValue: 1/119
    Software: GIMP 2.8.14
    SubSecCreateDate: 2016:04:17 16:25:24.890
    SubSecDateTimeOriginal: 2016:04:17 16:25:24.890
    SubSecModifyDate: 2017:01:22 13:30:20.890
    SubSecTime: 890
    SubSecTimeDigitized: 890
    SubSecTimeOriginal: 890
    Subject: [2016, 'April 2016', 'cameraphone', 'Everett', 'Galaxy S5', 'Lily', 'Madeleine', 'Violet']
    Technology: Cathode Ray Tube Display
    UserComment:
    ViewingCondDesc: Reference Viewing Condition in IEC61966-2.1
    ViewingCondIlluminant: 19.6445 20.3718 16.8089
    ViewingCondIlluminantType: D50
    ViewingCondSurround: 3.92889 4.07439 3.36179
    WhiteBalance: Auto
```
---
### Metadades: model de càmera
```
    Make: samsung
    Model: SM-G900V
```
![EXIF: Model de la càmera](metadata/img/exif_camera-model.png)

---
### Metadades: ubicació
```
    GPSPosition: 40 deg 42' 56.07" N, 73 deg 50' 36.35" W
    GPSAltitude: 18 m Below Sea Level
```
![EXIF: Ubicació](metadata/img/exif_gps.png)

---

### Altres metadades explícites

```
    CreatorTool: Aperture 3.6
    Software: GIMP 2.8.14
    Subject: [2016, 'April 2016', 'cameraphone', 'Everett', 'Galaxy S5', 'Lily', 'Madeleine', 'Violet']
```

---

## Com eliminar les metadades

---


### Esborrar metadades des d'un ordinador

- [MAT2](https://0xacab.org/jvoisin/mat2)([Web](https://matweb.info/), [Tails](https://tails.boum.org/doc/sensitive_documents/metadata/index.en.html), altres Linux per consola)
- [jExiftoolGui](https://github.com/hvdwolf/jExifToolGUI/releases) (Linux/Windows/Mac)

---

![Captura de la web del MAT2](metadata/img/mat2-web.png)

---

### Esborrar des d'Android

- ScrambledExif ([F-Droid](https://f-droid.org/packages/com.jarsilio.android.scrambledeggsif/) o [Google Play](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif))
- [Signal](https://signal.org/android/apk/) les sobreescriu i també deixa tapar cares.

---

**Scrambled Exif**

![Scrambled Exif](metadata/img/scrambled_captura.png)

---
**Signal 1/2**

![icona del signal per esborrar les cares](metadata/img/signal-blur-toolbar.jpg)

---

**Signal 2/2**

![esborrant les cares des del signal](metadata/img/nens_gelat_captura_signal.png)

---

### Resultat

Després d'esborrar les metadades d'una foto,
veiem que ja no hi ha informació compromesa.
```
user@host:~$ mat2 -s nens_gelat_neta.jpg
  No metadata found
```
---
#### Metadades: més info
* Metadata Anonymisation Toolkit: [Pàgina del projecte](https://mat.boum.org/)
* Código Sur: [Borrar metadatos y difuminar caras en fotos y videos](https://blog.codigosur.org/borrar-metadatos-y-difuminar-caras-en-fotos-y-videos)
* Eina exiftool: https://linux.die.net/man/1/exiftool

---
#### Reconeixement
* Nens adorables: [Joe Shlabotnik "Kids Eating Ice Cream On The School Steps" - CC by-nc-sa v2.0](https://www.flickr.com/photos/joeshlabotnik/31892072800/)

{{% /section %}}

---

{{% section %}}

### Anonimat a la xarxa: Tor
![Logo de Tor](tor/img/tor-logo.png)

---

#### Tor és
* Una **xarxa** d'ordinadors sobre internet per anonimitzar.
* El **programa** que connecta els ordinadors a la xarxa Tor.
* El **navegador** que funciona a través de la xarxa Tor.

---

#### Funcions
* Amaga l'adreça IP
* Xifra el trànsit
* Serveis onion

---

#### Contrapartides
* Retard i ample de banda limitat
* Crida l'atenció
* Bloquejos, traves

---

#### Adreça IP

**Destí** i **remitent** d'una comunicació a internet.

El nostre remitent és una **adreça pública**.

Pot ser la de:
* casa
* biblioteca
* local social
* …

---

#### Adreça IP

Les ISP tenen comprats **rangs** d'adreces IP, i estan repartits per **zones** geogràfiques. Per tant, la nostra adreça IP pública revela:
* La companyia telefònica contractada
* La localització física aproximada

---

#### Adreça IP
Podem esbrinar la informació que escampem per internet amb serveis com [ipleak.net](https://ipleak.net)
![Adreça IP](tor/img/ip-leak_clearnet.gif)

---

#### Internet ≠ WWW
A Internet hi funcionen molts protocols d'aplicació. Per conviure en la mateixa màquina, es fan servir ports TCP o UDP.

La Web fa servir els següents ports
* HTTP: TCP-80
* HTTPS: TCP-443
* Resolució de noms (DNS): UDP-53

---

#### Internet ≠ WWW
Però n'hi ha molts més, de protocols
* Xat (Jabber): TCP-5222
* Correu: TCP-993, TCP-587, TCP-465
* Age of Empires II: TCP-47624

---

#### Internet ≠ WWW
Actualment la tendència és definir nous protocols per sobre de HTTP: les **API web**
* API de Twitter
* Xat (Matrix.org, RocketChat)
* Xarxes socials lliures (federació entre servidors)

---

#### Connexió HTTP, sense xifrar
![Connexió HTTP](tor/img/internet_16-9.png)

---

#### Connexió xifrada, TLS.
Xifra la connexió entre client i servidor.
* Web, mail, xat, …

Els túnels TLS ens protegeixen d'atacs:
* **Passius**: robar credencials, llegir formularis i missatges
* **Actius**: manipular webs, programes, actualitzacions.

---

#### Connexió HTTPS, xifrada

![Connexió HTTPS](tor/img/tls-internet_16-9.png)

---

#### Adreça IP del remitent **sense** usar **Tor**
Des de Barcelona i amb Movistar:
![Adreça IP sense Tor](tor/img/ip-leak_clearnet.gif)

---

#### Adreça IP del remitent **usant Tor**
Des de Barcelona i amb Movistar:
![Adreça IP amb Tor](tor/img/ip-leak_tor.gif)

---

#### Connexió a través de Tor
![Connexió amb Tor](tor/img/tor-internet_16-9.png)

---

#### Nodes de sortida a Clearnet

* Pocs: ample de banda limitat.
* Perillosos: Imprescindible HTTPS.

---

#### Connexió a través de Tor
![Connexió amb Tor i TLS](tor/img/tor-tls-internet_16-9.png)

---

#### Serveis onion

Comunicació entre desconeguts
* Servidor anònim.
* Usuària anònima.

En comparació a serveis a la clearnet:
* No congestiona els nodes de sortida.
* No necessita HTTPS.

---

#### Alguns llocs web onion

* [Hidden Wiki](http://zqktlwiuavvvqqt4ybvgvi7tyo4hjl5xgfuvpdf6otjiycgwqbym2qad.onion/wiki/) (llista de llocs onion)
* [Torch](http://xmh57jrknzkhv6y3ls3ubitzfqnkrwxhopf5aygthi7d6rplyvk3noyd.onion) (cerca llocs onion)
* ["Imperial Library"](http://kx5thpx2olielkihfyo4jgjqfb7zx7wxr3sd4xzt26ochei4m6f7tayd.onion/) (biblioteca epub/pdf)
* [Njal.la](http://njallalafimoej5i4eg7vlnqjvmb6zhdh27qxcatdn647jtwwwui3nad.onion/) (dominis, servidors)
* [Facebook](https://www.facebookcorewwwi.onion/) (en serio!)

---

#### Aplicacions basades en serveis onion
* [OnionShare](http://lldan5gahapx5k7iafb3s4ikijc4ni7gx5iywdflkba5y2ezyg6sjgyd.onion/): envia arxius grans
* [Briar](https://briarproject.org/): comunicació per Tor o offline
* [Wahay](https://wahay.org/): audio-conferències segures

---

#### Aplicacions torificables:
* **Navegador**: Firefox → Tor Browser Bundle
* **Altres** (Thunderbird, Telegram, Pidgin, ... )
    1. Executa **Tor Browser** i espera que connecti
    2. Executa l'**aplicació** que suporta SOCKS5
    3. Configuració → xarxa → "servidor intermediari"
        - tipus **SOCKS5**
        - servidor: `localhost` o bé `127.0.0.1`
        - port: `9150`


{{% /section %}}

---

{{% section %}}

## La navalla suïssa: Tails

![logo de TAILS](tails/img/logo.png)

_El sistema "live" incògnit i anònim_

---

## Amnèsic
* No deixa rastre a l'ordinador
* No deixa rastre al pendrive, excepte:  
* La _partició de persistència_  
  (voluntària, xifrada, mínima)

---

## Incògnit
* Tot el trànsit de xarxa va per Tor
* Millores de privacitat al navegador
* Eines de criptografia comunes

---

## Aplicacions

![Tor](tails/img/apps/tor-browser.png)
![KeepassXC](tails/img/apps/keepassxc.png)
![MAT2](tails/img/apps/mat2.png)
![Veracrypt](tails/img/apps/veracrypt.png)
![LUKS](tails/img/apps/luks.png)
![Gimp](tails/img/apps/gimp.png)
![Inkscape](tails/img/apps/inkscape.png)
![Libre Office](tails/img/apps/libreoffice.png)

---

## Instaŀlació a un pendrive

![Gnome Disks abans d'escriure una imatge al pen](tails/img/gnome_disks_drive.png)

---

## Arrencada des de la BIOS/UEFI

---

| Fabricant  | Tecles | Fabricant  | Tecles |
| ---------- | ------ | ---------- | ------ |
| Acer | F12, F9, F2, Esc | Apple  | Option |
| Asus       | Esc    | Clevo      |     F7 |
| Dell       | F12    | Fujitsu  | F12, Esc |
| HP         | F9     | Huawei     | F12    |
| Intel      | F10    | Lenovo     | F12    |
| MSI        | F11 | Samsung | Esc, F12, F2 |
| Sony | F11, Esc, F10 | Toshiba   | F12    |

---

![boot-menu](tails/img/boot_menu.png)

---

## Espai persistent i xifrat

![Gnome Disks mostrant la partició de persistència](tails/img/gnome_disks_luks.png)

---

### Funcionalitats "persistibles"

* Carpeta per dades personals
* Opcions de la pantalla d'inici
* Adreces d'interès
* Connexions de xarxa
* Software addicional
* Thunderbird
* GnuPG (claus)
* Pidgin

---

## Per saber-ne més

* Consulta [la wiki de Tails](https://tails.boum.org/doc/index.es.html)
* Llegeix les [advertències de seguretat generals](https://tails.boum.org/doc/about/warning/index.es.html)
* Llegeix les [precaucions sobre la persistència](https://tails.boum.org/doc/first_steps/persistence/warnings/index.es.html)
* Instaŀla'l i juga! :D

{{% /section %}}

---

{{% section %}}

## Navegació Anònima: RiseUPVPN
![LogoRiseupVPN](riseupvpn/img/logo.png)

---

![Captura RiseupVPN Android](riseupvpn/img/screenshot1.jpeg)

---

### Per què serveix?
- Amaga a la nostra operadora i aliats els detalls de la nostra activitat a internet.
- Tot el trànsit que generem passa per Riseup primer. Després està fora de control espanyol.
- Evita atacs propers (wifi pública) amb un túnel xifrat fins Riseup.
- Evita censura del teu país.

---

### PROS
- Software Lliure
- Disponible en totes les plataformes (Android, Linux, Mac , Windows )
- Fàcil de fer servir
- Proveïdor de confiança

---

### CONTRES

- Velocitat d'internet reduïda
- Tot el trànsit passa pel mateix punt. Si cau riseup caiem totes (?)
- Com Tor i altres eines, pot generar sospites. Però millor que confirmar-les!

{{% /section %}}

---

{{% section %}}

## Complements per Firefox

![Logo firefox](firefox-addons/img/firefox.png)

---
#### Necessitat
La navegació per internet no és privada ni segura per defecte:
* Pàgines sense HTTPS per defecte
* Anuncis basats en els teus hàbits de navegació
* Cookies amb dates de caducitat molt llunyanes
* Programes que monitoren la nostra activitat a les pàgines que visitem

---
#### HTTPS Everywhere
![Logo HTTPS Everywhere](firefox-addons/img/https-everywhere.png)

* Prova la comunicació xifrada (HTTPS) si el servidor la suporta.
* Té un mode de bloquejar totes les connexions per HTTP sense xifrar.
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/https-everywhere/)

---
#### uBlock Origin
![Logo uBlock Origin](firefox-addons/img/ublock-origin.png)

* Bloqueja els anuncis de manera eficient i permet crear els teus propis filtres
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/ublock-origin/)

---
#### Cookie-AutoDelete
![Logo cookie-auto-delete](firefox-addons/img/cookie-auto-delete.png)
* Destrueix les cookies de qualsevol pestanya que no tinguis oberta
* [Descarrega'l](https://addons.mozilla.org/ca/firefox/addon/cookie-autodelete/)

---
#### Privacy badger
![Logo Privacy Badger](firefox-addons/img/privacy-badger.png)

* Bloqueja els continguts de tercers que tinguin l'objectiu de rastrejar
* [Descarrega'l](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)


{{% /section %}}

---

{{% section %}}

### Gestió de credencials: KeepassX
![KeepassX logo](keepassx/img/keepassx_logo.png)

---
#### Els 3 manaments de les contrasenyes
* _No les reutilitzaràs_
* _No les compartiràs_
* _No les faràs febles_

---
#### Propietats
* Caràcters diferents
* **Llargues**
* Difícils d'endevinar
    * Patrons evidents no
    * Dades conegudes no: noms, dates, aficions, etc.

---
#### Gestió ideal de les contrasenyes
* Una per cada compte → moltes!
* Difícils d'endevinar → aleatòries!
* Difícils de trencar a "força bruta" → llargues!

---
#### Exemples
Exemples de menys a més fortes:
* Indigna: `tkmamor`
* Txepi-txepi: `tkmuchoMIam0r!<3`
* Idestructible:
  `mi corazon palpita como una patata frita tutum tutum`

---
#### Les matemàtiques darrere
Millor invertir en longitud que en caràcters diferents.

* **x**: Possibilitats per cada caràcter (només minúscules: 26)
* **y**: Longitud en caràcters
* **z**: Nombre total de possibles contrasenyes

  ![z = x^y](keepassx/img/possibilitats.png)
* **H**: Entropia (complexitat). Objectiu: més de 100 bits

   ![H = log2(z) = y*log2(x)](keepassx/img/entropia.png)

---
#### Representació en 3D
![gràfic longitud vs símbols](keepassx/img/entropia3d.png)

---
#### Gestió habitual de les contrasenyes
* Una per TOTS els comptes
* Fàcil d'endevinar → aniversaris, noms propers, …
* Fàcil de trencar → pocs caràcters

**TENIM UN PROBLEMA**

---
#### Gestor de contrasenyes
* Base de dades xifrada
* Una contrasenya per dominar-les a totes
* Generador de contrasenyes fortes

---
#### Com fer-la servir
* Organització
    * per grups, subgrups i entrades
    * entrades desordenades i buscador
* Fer còpies de seguretat

---
* Tutorial a Código Sur (2020): [KeePassXC: Almacenar y generar contraseñas de manera segura](https://blog.codigosur.org/keepassxc-almacenar-y-generar-contrasenas-de-manera-segura/)
* Tutorial a Surveillance Self Defense: [Cómo usar KeepassX](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassx)
* Tutorial a Security in a Box: [KeePass – Almacenamiento seguro de contraseñas](https://securityinabox.org/es/keepass_main)
* Calculadora de robustesa de contrasenyes: [passfault](https://passfault-hrd.appspot.com/)


{{% /section %}}

---

{{% section %}}

### El meu veí Google

![pancarta de fuckoffgoogle a berlin](cercadors/img/google.jpg)

---
#### El monstre de Google
Google ens rastreja i ens estudia com ratolins maŀleables.
* Amb l'historial de cerca (Google)
* Amb l'historial de navegació web (Chrome)
* Amb els anuncis en altres llocs (AdSites)
* Amb anàlisi per admins de webs (Analytics)
* Amb el mòbil (Ubicació, Aplicacions, Contactes, Mail)
* … tot?

Més informació a [GAFAM si pots!](https://gafam-si-pots.hotglue.me/google/)

---
#### Cercadors

* NO **Google**, trafica amb dades, és un règim dictatorial digital
* NO **Bing**, Microsoft trafica amb dades, és un règim dictatorial digital
* NO **Yandex**, trafica amb dades, és un règim dictatorial digital
* NO **Ecosia**, planta arbres a canvi de vendre la teva identitat als mercats de tràfic de dades, màrketing i manipulació social
* NO **StartPage/Ixquick**, El va comprar una empresa de Big Data.

---

#### Cercadors
* SÍ [Duckduckgo](https://duckduckgo.com/) Cercador. USA. Anuncis no personalitzats.
* SÍ [Qwant](http://qwant.com/) Cercador. FR. Anuncis no personalitzats.
* SÍ Searx. Metacercador. Lliure.
   * [searx.me](https://searx.me)
   * [trovu (komun)](https://trovu.komun.org/)
   * [search.fuckoffgoogle.net](https://search.fuckoffgoogle.net/)
   * [searx.laquadrature.net/](https://searx.laquadrature.net/)

---
#### Correu

* Coŀlectius activistes, per donacions
    * [es] [SinDominio](https://sindominio.net)
    * [it] [Autistici/Inventati](https://autistici.org)
    * [us] [RiseUp](https://riseup.net)
    * [us] [Aktivix](https://aktivix.org/)

---
#### Correu

* Cooperatives, associacions i empreses properes
    * [de] [Posteo](https://posteo.de/)
    * [cat] [Pangea](https://pangea.org)
    * [cat] [La mar de bits](https://lamardebits.org)
    * [cat] [Maadix](https://maadix.net)

---
#### Correu

Llistes més completes:
* [Riseup: Radical servers](https://riseup.net/en/security/resources/radical-servers)
* [Privacy Tools](https://www.privacytools.io/providers/email/)
* [Privacy-Conscious Email Services](https://www.prxbx.com/email/)

---

#### "El Drive"

* Google ho indexa tot
* Censura indiscriminadament:
  * treballadores sexuals
  * dissidents polítics
  * "errors"
* Ens crea més dependència a internet

---
#### Arxius: Solucions offline
* Pendrives (sempre tenen bateria i cobertura)

![pen en forma de nina russa](cercadors/img/pendrive.jpg)

---
#### Arxius: Solucions online
* Serveis d'enviament caduc
  * [share.riseup.net](https://share.riseup.net)
  * [lufi (disroot)](https://upload.disroot.org/)
  * Llista: [alt framadrop](https://alt.framasoft.org/en/framadrop)

---
#### Arxius: Solucions online
*  Nextcloud
  * [Proveïdores comercials](https://nextcloud.com/providers/)
  * [Maadix](https://maadix.net/)
  * [Pangea](https://pangea.org/)
  * [Disroot](https://disroot.org/es/services/nextcloud)
  * [Indie hosters](https://indie.host/)

---
#### Documents: solucions offline

LibreOffice (encara funciona! I millor que abans!)

![els logos de les 5 aplicacions de libreoffice](cercadors/img/libreoffice.jpg)

---
#### Documents: solucions online
* [ProtectedText](https://www.protectedtext.com/): xifrat amb una contrasenya i edició separada
* [CryptPad](https://cryptpad.fr/): xifrat però públic amb enllaç
* [Nextcloud Text](https://apps.nextcloud.com/apps/text): coŀlaboració en temps integrada
* CodiMD [demo gratis](https://demo.codimd.org) o [registre](https://hackmd.io/)
* Nextcloud + [Collabora Online](https://nextcloud.com/collaboraonline/): edita ODT online
* Nextcloud + [OnlyOffice](https://nextcloud.com/onlyoffice/): edita DOCX online
* [JetPad](https://jetpad.net/): amb formatat i coŀlaboració instantània
* EtherPad. Els clàssics, fàcils, públics:

---
#### Documents: solucions online
* EtherPad
   * [Riseup](https://pad.riseup.net/)
   * [Codigosur](https://pad.codigosur.org/)
   * [Vedetas](https://antonieta.vedetas.org)
   * [Framasoft](https://mensuel.framapad.org/)
   * [Kefir](https://pad.kefir.red/)
   * [Disroot](https://pad.disroot.org/)

---
#### Mapes: OpenStreetMaps
La wikipèdia dels mapes
* [Estàndard amb 3 capes](https://www.openstreetmap.org/way/157016134)
* [Opentopomap](https://opentopomap.org/#map=17/41.44536/2.15773): corbes de nivell i muntanya
* Umap: crea un mapa personalitzat
   * [FramaSoft](https://framacarte.org)
   * [OSM-FR](http://umap.openstreetmap.fr)

---
#### Mapes: OpenStreetMaps
* Per a mòbil
  * [OsmAnd](https://fossdroid.com/a/osmand~.html) Complet, més configurable
  * ["Maps"](https://fossdroid.com/a/maps.html) Complet, més ràpid

---
#### Mapes: Institut Cartogràfic i Geològic de Catalunya

* [Instamaps](https://www.instamaps.cat/): crea un mapa personalitzat
* [Mapa polític/topogràfic i de satèl·lit](http://www.icc.cat/vissir3/index.html?ryKiLHg78)
* [Altres](http://betaportal.icgc.cat/)

---
#### Mapes: Rutes/navegació
* Indicacions
   * [Mou-te](https://mou-te.gencat.cat): Amb transport públic, a catalunya
   * [OpenStreetMaps](https://www.openstreetmap.org/directions?engine=graphhopper\_bicycle\&route=41.3464%2C1.6995%3B41.5113%2C1.7016#map=11/41.4286/1.7104): A peu, bici o cotxe
   * [GrassHopper](https://graphhopper.com/maps/) Vehicles privats, ruta multipunt
* Navegació
  * [OsmAnd](https://fossdroid.com/a/osmand~.html) Complet, més configurable
  * ["Maps"](https://fossdroid.com/a/maps.html) Complet, més ràpid

---
### Ànims! :D
_No ho farem ni tot sols, ni tot de cop_

[![imatge de complicitat de dos joves de dibuixos animats amb un smartphone](cercadors/img/nothing-to-hide.jpg)](https://sindominio.net/logout/nothing-to-hide/)

_Sinó pas a pas i agafades de la mà_


{{% /section %}}

---

{{% section %}}

### Gràcies per l'atenció!
Estem en contacte!
* 📧  **Mail** [logout(a)sindominio.net](mailto:logout@sindominio.net)
* 🌏  **Web** [sindominio.net/logout](https://sindominio.net/logout/web)
* 📌  **AFK** [Can Vies](https://www.openstreetmap.org/way/262218049)

{{% /section %}}
