+++
outputs = ["Reveal"]
+++

{{% section %}}

# Encriptació de dispositius #

---


#### Índex

- Intro: Què és l'encriptació i com funciona?
- Guia bàsica de com encriptar dispositius android
  - genèric
  - xiaomi
- Encriptació d'un pendrive
  - amb LUKS
  - amb Veracrypt
- Xifrat de/en sistemes operatius
  - Linux, Mac, Windows
- Xifrat de mail _extrem a extrem_

---


## Hola!
* Autoria: 2020 Hacklab Logout
* [Llicència: CC - Reconeixement i Compartir igual](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)

---

## Roda de motivacions

«Què esperes d'aquesta sessió?»

«Per què estàs aquí?»

---

## Sondeig tècnic

* Mail: gmail - riseup
* OS: windows - ubuntu
* Anon: Tor, metadades, cares
* Mòbil: precaucions?

---

## Seguretat
* Relativa al context
* De grup
* Psico-social + física + info
* Progressiva
* Objectiu: activisme sostenible


{{% /section %}}

---

{{% section %}}

# Introducció #

---


## Que es la encriptació/xifrat ##

Es el process de convertir una informació en un codi que idealment nomes pot ser interpretat per qui te la clau. Es fa per tal  d'evitar acces no autoritzat a informació xifrada.

---


#### Exemple mes basic de xifrat, el xifrat del cesar ####
- ningu podra desxifrar aquest codi -> qlqjx srgud ghvaliudu dtxhvw frgl 
![cesar](Xifrat/cesar.png) <!-- .element: style="width: 60%;margin: 0 0;" -->


---


### Llocs on es pot aplicar el xifrat ###

- Durant el transport (Xifrat punt a punt, xifrat d'enllaç)
- Xifrat en l'emmagatzamamnet/de dispositius

---

#### Xifrat punt a punt

![endtoend_transport_diagram](Xifrat/end-to-end-encryption-comparison.jpg)

---

#### Xifrat de disc dur

![disk_encryption_diagram](Xifrat/diskencryption.png)

{{% /section %}}

---

{{% section %}}

# Com encriptar un dispositiu android #

---


# Passos previs #
- Carregar el mòbil al màxim
- **!! Deixar el mòbil endollat durant tot el procés !!**
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password
- **Recorda** Aquests passos són susceptibles de canviar, segons la versió d'Android

---


<div class="left">

En primer lloc anem a _Ajustes_

</div>

<div class="right">

![step1](EncryptAnd/Step1.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

---


<div class="left">

Dins de setting, anem a a la secció de seguretat.

</div>
<div class="right">

![step2](EncryptAnd/Step2.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

---


<div class="left">

Dins de seguretat busquem una secció que s'anomeni xifrat o encriptació.
Cliquem en Encriptar telèfon

</div>
<div class="right">

![step3](EncryptAnd/Step3.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

---


<div class="left">

Donem al botó de Encriptar dispositiu.
Ens demanarà que confirmem la contrasenya, un cop introduïda començarà el procés d'encriptació.

</div>
<div class="right">

![step4](EncryptAnd/Step4.jpg) <!-- .element: style="width: 70%;margin: 0 0;" -->

</div>

---

# Dispositiu encriptat #

{{% /section %}}

---

{{% section %}}
# Com encriptar un dispositiu android (Xiaomi) #

---


# Passos previs #
- Carregar el mòbil al màxim
- !! Deixar el mòbil endollat durant tot el procés !!
- Fer còpia de seguretat de totes les dades
- Canviar el bloqueig a tipus password

---

<div class="left">

En primer lloc anem a _Ajustes_

</div>
<div class="right">

![xstep0](EncryptXiaomi/Step0.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

Després anem a la secció de sobre el telèfon, per tal d'activar les _opcions de desenvolupador_.

</div>
<div class="right">

![xstep1](EncryptXiaomi/Step1.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

Aquí premem sobre _Versión MIUI_, fins que aparegui un cartell confirmant-nos que les opcions de desenvolupador han estat activades

</div>

<div class="right">

![xstep2](EncryptXiaomi/Step2.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

Un cop activades les opcions de desenvolupador, tornem a la secció general de configuració. Aquí anem a _Ajustes adicionales_

</div>
<div class="right">

![xstep3](EncryptXiaomi/Step3.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

En _Ajustes adicionales_ anem a _opcions de desenvolupador_.

</div>
<div class="right">

![xstep4](EncryptXiaomi/Step4.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

- Dins de _opcions de desenvolupador_ fem scroll fins abaix del tot.
- Seleccionem l'última opció, _Cifra tu dispositivo usando la contraseña de bloqueo_

</div>
<div class="right">

![xstep5](EncryptXiaomi/Step5.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

<div class="left">

- En aquesta secció activem l'únic boto que hi ha.
- Ens demanarà que confirmem el password del telefon, un cop fet començarà l'encriptació del dispositiu

</div>

<div class="right">

![xstep6](EncryptXiaomi/Step6.jpg) <!-- .element: style="width: 60%;margin: 0 0;" -->

</div>

---

# Dispositiu encriptat #

{{% /section %}}

---

{{% section %}}

# Com encriptar un pendrive (amb LUKS) #

---


Instalar _cryptsetup_, si encara no esta instalat. Una posible manera és amb la següent comanda:

```
user@host:~$ sudo apt install cryptsetup
```

---


Trobar el pendrive que es vol encriptar amb la comanda _lsblk_.

![](./EncryptPen/lsblk.png)

A l'esquerra del tot, es distingeixen dos unitats, _sda_ i _sdb_ en aquest cas en particular.

El pendrive que s'encriptara és de 16GB. Mirant a la columna anomenada SIZE es veu que _sda_ és de 477GB i que _sdb_ de 14.5GB, per lo que el nostre pendrive és _sdb_ en aquest cas.

---

ara, suposant que el vostre pendrive és _sdb_

```
umount /dev/sdb1 
sudo cryptsetup luksFormat /dev/sdb
```

per encriptar el dispositiu. Si el vostre volum sigues sdc o sdd o qualsevol altre, s'han de escriure les mateixas comandes pero canviant _sdb_ pel que pertoqui.

**Atencio! Tota la informació del pendrive es perdera.**

---


En aquest pas s'ha d'especificar la contrasenya per desencriptar el dispositiu

![](./EncryptPen/luksFormat.png)

---

Falten només dos pasos!

```
sudo cryptsetup luksOpen /dev/sdb <nom_qualsevol>
```

on <nom_qualsevol> pot ser qualsevol seqüència de caracters que volgeu.

---


Per a crear una partició on guardar arxius (encriptats)

```
mkfs -t ext4 -L "<el_teu_nom>" /dev/mapper/<nom_qualsevol>
```

on <nom_qualsevol> ha de ser el mateix que hem ficat en el pas anterior

i <el_teu_nom> és el nom que tindrà el pendrive. Poden ser el mateix nom.

---


#### Pendrive encriptat!

Simplement treieu-lo de l'ordinador i torneu-lo a ficar i us demanara la contrasenya per a desencriptar.

{{% /section %}}

---

{{% section %}}

# Com encriptar un pendrive (amb Veracrypt) #

---

### Instal·lar Veracrypt

Link: https://www.veracrypt.fr/en/Downloads.html

---

Des de VeraCrypt, clicar "Create Volume"

![](./EncryptPen/create_volume.png)

---


Clickar a la segona opció i "Next"

![](./EncryptPen/pas1.png)

---


Qualsevol de les dues opcions es correcta. La que preferiu

![](./EncryptPen/pas2.png)

---


Clickar a "Select Device..."

![](./EncryptPen/pas3.png)

---


Asegureu-vos de triar el vostre pendrive i no cap altre cosa!

Podeu esborrar arxius que no voleu esborrar si no aneu am compte aqui

![](./EncryptPen/pas4.png)

---


Simplement "Next"

![](./EncryptPen/pas5.png)

---


Revisar que heu fet una copia de seguretat d'allò que tenieu abans el pendrive, i "Yes".

![](./EncryptPen/pas6.png)

---


Pot ser demana la contrasenya d'administrador de l'ordinador, la que feu servir per entrar a la vostra sesió. Ficar la contra, "OK" i "Next".

![](./EncryptPen/pas7.png)

---


"Next".

![](./EncryptPen/pas8.png)

---


Posar la contrasenya que voelu fer servir per desencriptar el volum.

Que sigui llarga!

![](./EncryptPen/pas9.png)

---


Segurament us interesa la primera opció. Si no ho teniu clar, clickeu la segona. Després "Next".

![](./EncryptPen/pas10.png)

---


"Next"

![](./EncryptPen/pas11.png)

---


Mogeu el ratoli fins que us canseu, i després clickeu a "Format".

![](./EncryptPen/pas12.png)

---


Esteu apunt d'esborrar tot lo que tenieu al pendrive. Si esteu segures, "Yes".

![](./EncryptPen/pas13.png)

---


#### Volum encriptat!

"Exit" i podeu treure el pendrive quan volgeu.

![](./EncryptPen/pas14.png)

---


Cada cop que volgeu desencriptar el pendrive, torneu-lo a ficar. Inicieu VeraCrypt i clickeu "Auto-Mount Devices".

![](./EncryptPen/auto_mount.png)

---


Fiqueu la contrasenya que heu creat anteriorment, i "OK".

Es posible que torni a demanar la contrasenya d'administrador del vostre ordenador.

Pot trigar un minut en desencriptar el pendrive. Quan acabi, s'obrira automaticament la carpeta on podeu guardar arxius encriptats.

![](./EncryptPen/contra.png)

{{% /section %}}

---

{{% section %}}

# Xifrat de disc de dades i de sistema ##

- Linux
- Mac
- Windows

---


## Xifrat multiplataforma ##

- Es poden crear _volums_ xifrats amb [Veracrypt](https://www.veracrypt.fr/en/Home.html)
  - dispositius sencers (pendrive)
  - una carpeta (volum virtual)
  - una partició
  - tot el sistema (només windows, equivalent a LUKS)

---


## Xifrat Linux ##

- Es fa durant la instal·lació
  - [LUKS/cryptsetup](https://gitlab.com/cryptsetup/cryptsetup): tot el sistema ([tutorial](https://forums.linuxmint.com/viewtopic.php?t=298852))
  - [eCryptfs](https://www.ecryptfs.org/): només la carpeta personal

---


## Xifrat Mac ##
- Per xifrar el disk mac fa servir un programa anomenay FileVault
- [Documentación oficial de apple](https://support.apple.com/en-us/HT204837)

---


## Xifrat Windows ##
- Bitlocker: xifrat de tot el disc ([Manual oficial](https://support.microsoft.com/en-us/windows/turn-on-device-encryption-0c453637-bc88-5f74-5105-741561aae838))
- [Veracrypt](https://www.veracrypt.fr/en/Home.html): sistema sencer o "volums".

{{% /section %}}

---

{{% section %}}

# Xifrat de correu electrònic ##

---

## Context

- El emails es xifren per TLS, però només entre dispositiu i servidor
- Google pot llegir els emails dels comptes gmail

---

## Solucions

- Per xifrar _extrem a extrem_: [GPG](https://gnupg.org/) (claus asimètriques)
- Per fer-lo servir: [Thunderbird](https://support.mozilla.org/es/kb/openpgp-thunderbird-howto-and-faq)
- _amb versions anteriors calia instal·lar l'extensió [Enigmail](https://enigmail.net/)_

---

## Més recursos

- [Email self defense](https://emailselfdefense.fsf.org/es/)
- Milpa digital: [Cifrando correos con GPG](https://milpadigital.org/milpadigital-10)

![retall de la infografia milkpa digital: dona xifrant correus al portàtil](Xifrat/milpa-gpg.png)

{{% /section %}}
